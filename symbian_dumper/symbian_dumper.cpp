#include <e32hal.h>
#include <e32rom.h>
#include <e32svr.h>
#include <hal.h>

#include "common.h"

TBool dump_to_file(const TDesC& dump_name, TUint32& address, TUint32& size)
{
	// The amount of memory to be written in one go.
	TUint32 dump_chunk_size = 0x1000;

	// Connect to the file server.
	RFs session;
	TInt result = session.Connect();

	if (result != KErrNone)
	{
		_LIT(KConnectionFailure, "Failed to connect to the file server during dumping:%d\n");
		logger->log(KConnectionFailure, result);
		logger->get_char();
		User::Leave(result);
	}

	// Create the dump file.
	RFile dump_file;
	result = dump_file.Replace(session, dump_name, EFileShareExclusive | EFileWrite);

	if (result != KErrNone)
	{
		_LIT(KFileOpenFailure, "Failed to open file for dumping:%d\n");
		logger->log(KFileOpenFailure, result);
		logger->get_char();
		User::Leave(result);
	}

	// Check that we have at least dump_chunk_size bytes of memory available before beginning.
	TInt memory_available;
	User::Available(memory_available);
	
	if (static_cast<TUint32>(memory_available) < dump_chunk_size)
	{
		_LIT(KNotEnoughMemory, "Not enough memory!\nRequired:%dB\nAvailable:%dB\n");
		logger->log(KNotEnoughMemory, dump_chunk_size, memory_available);
		logger->get_char();
		User::LeaveNoMemory();
	}

	// Allocate memory
	TUint8* dump_mem = reinterpret_cast<TUint8*>(User::Alloc(dump_chunk_size));

	if (!dump_mem)
	{
		_LIT(KMemoryAllocationFailure, "Failed to allocate memory for dumping!\n");
		logger->log(KMemoryAllocationFailure);
		logger->get_char();
		User::LeaveNoMemory();
	}

	TPtr8 dump_mem_ptr(dump_mem, dump_chunk_size, dump_chunk_size);

	// We use these for determining the progress
	TInt total_chunks = size / dump_chunk_size;
	TInt current_chunk = 0;

	// Dump the chunk
	while (size)
	{
		if (dump_chunk_size > size)
		{
			dump_chunk_size = size;
		}

		Mem::Copy(reinterpret_cast<TAny*>(dump_mem), reinterpret_cast<TAny*>(address), dump_chunk_size);

		size -= dump_chunk_size;
		address += dump_chunk_size;

		// Print a line visible only in the console that indicates how many percent is done
		_LIT(KProgress, "Progress: %d");
		_LIT(KPercent, "%%");
		logger->console_line(KProgress, (100 * current_chunk++) / total_chunks);
		logger->console_raw(KPercent);

		dump_file.Write(dump_mem_ptr, dump_chunk_size);
	}

	// Make sure the progress message displays 100% and starts a new line
	_LIT(KProgress100, "Progress: 100");
	_LIT(KPercent, "%%\n");
	logger->console_line(KProgress100);
	logger->console_raw(KPercent);

	// Free the memory that we allocated for dumping
	User::Free(dump_mem);

	// Close the file server session and the file handle.
	dump_file.Flush();
	dump_file.Close();
	session.Close();

	return true;
}

TBool dump(const TDesC8& name, TUint32 address, TUint32 size)
{
	_LIT8(KDumpFormat, "E:\\%S-%X.dmp");

	TInt model;
	HAL::Get(HALData::EModel, model);

	TBuf8<256> dump_name;
	dump_name.Format(KDumpFormat, &name, model);

	TBuf16<256> wdump_name;
	wdump_name.Copy(dump_name);

	return dump_to_file(wdump_name, address, size);
}

void dump_ROM()
{
	_LIT(KDumpingROM, "Dumping the ROM...\n");
	logger->log(KDumpingROM);

	// The ROM begins with the TRomHeader, so we can easily get the ROM size.
	TUint32 rom_base = UserSvr::RomHeaderAddress();
	TUint32 rom_size = reinterpret_cast<TRomHeader*>(rom_base)->iRomSize;

	_LIT(KROMBase, "ROM base:0x%08X\n");
	_LIT(KROMSize, "ROM size:0x%08X\n");
	_LIT(KROMRootDirectory, "ROM root dir:0x%08X\n");

	logger->log(KROMBase, rom_base);
	logger->log(KROMSize, rom_size);
	logger->log(KROMRootDirectory, UserSvr::RomRootDirectoryAddress());

	_LIT8(KROMDumpNameFormat, "ROM-0x%X");

	TBuf8<256> dump_name;
	dump_name.Format(KROMDumpNameFormat, rom_base);

	if (!dump(dump_name, rom_base, rom_size))
	{
		_LIT(KROMDumpFailure, "Failed to dump the ROM!\n\n");
		logger->log(KROMDumpFailure);
	}
	else
	{
		_LIT(KROMDumpSuccess, "ROM successfully dumped!\n\n");
		logger->log(KROMDumpSuccess);
	}
}

void dump_BOOT()
{
	_LIT8(KBOOTDumpName, "BOOT");
	_LIT(KDumpingBOOT, "Dumping BOOT...\n");
	logger->log(KDumpingBOOT);

	if (!dump(KBOOTDumpName, 0x0, 0x2000))
	{
		_LIT(KBOOTDumpFailure, "Failed to dump BOOT!\n\n");
		logger->log(KBOOTDumpFailure);
	}
	else
	{
		_LIT(KBOOTDumpSuccess, "BOOT succesfully dumped!\n\n");
		logger->log(KBOOTDumpSuccess);
	}
}

void dump_ROOT()
{
	_LIT8(KROOTDumpName, "ROOT");
	_LIT(KFindingROOTSize, "Finding ROOT page size...\n");
	_LIT(KDumpingROOT, "Dumping ROOT...\n");
	_LIT(KROOTAddress, "ROOT address:0x%08X\n");
	_LIT(KROOTSize, "ROOT size:0x%X\n");

	logger->log(KFindingROOTSize);

	TUint root_dir = UserSvr::RomRootDirectoryAddress();
	TRAPD(root_size, try_find_page_size(root_dir));

	logger->log(KDumpingROOT);
	logger->log(KROOTAddress, root_dir);
	logger->log(KROOTSize, root_size);

	if (!dump(KROOTDumpName, root_dir, root_size))
	{
		_LIT(KROOTDumpFailure, "Failed to dump ROOT!\n\n");
		logger->log(KROOTDumpFailure);
	}
	else
	{
		_LIT(KROOTDumpSuccess, "ROOT succesfully dumped!\n\n");
		logger->log(KROOTDumpSuccess);
	}
}

void print_version(TVersion version)
{
	_LIT(KMajor, "Major:%d\n");
	_LIT(KMinor, "Minor:%d\n");
	_LIT(KBuild, "Build:%d\n\n");

	logger->log(KMajor, version.iMajor);
	logger->log(KMinor, version.iMinor);
	logger->log(KBuild, version.iBuild);
}

void print_machine_info()
{
	TPckgBuf<TMachineInfoV1> machine_info_buffer;
	UserHal::MachineInfo(machine_info_buffer);
	TMachineInfoV1& machine_info = machine_info_buffer();

	_LIT(KROMVersion, "ROM version:\n");
	_LIT(KDisplaySize, "Display size:\n");
	_LIT(KPixelWidth, "Width:%dpx\n");
	_LIT(KPixelHeight, "Height:%dpx\n\n");
	_LIT(KPhysicalScreenSize, "Physical screen size:\n");
	_LIT(KWidth, "Width:%d\n");
	_LIT(KHeight, "Height:%d\n\n");
	_LIT(KDisplayOffset, "Display offset:\n");
	_LIT(KX, "X:%d\n");
	_LIT(KY, "Y:%d\n\n");
	_LIT(KXYInputInfo, "XY input info:\n");

	logger->log(KROMVersion);
	print_version(machine_info.iRomVersion);

	TSize display_size = machine_info.iDisplaySizeInPixels;
	logger->log(KDisplaySize);
	logger->log(KPixelWidth, display_size.iWidth);
	logger->log(KPixelHeight, display_size.iHeight);

	TSize physical_screen_size = machine_info.iPhysicalScreenSize;
	logger->log(KPhysicalScreenSize);
	logger->log(KWidth, physical_screen_size.iWidth);
	logger->log(KHeight, physical_screen_size.iHeight);

	TPoint display_offset = machine_info.iOffsetToDisplayInPixels;
	logger->log(KDisplayOffset);
	logger->log(KX, display_offset.iX);
	logger->log(KY, display_offset.iY);

	TXYInputType XY_input_type = machine_info.iXYInputType;
	logger->log(KXYInputInfo);

	if (XY_input_type != EXYInputNone)
	{
		_LIT(KType, "Type:");
		logger->log(KType);

		if (XY_input_type == EXYInputPointer)
		{
			_LIT(KPointer, "pointer\n");
			logger->log(KPointer);
		}
		else if (XY_input_type == EXYInputMouse)
		{
			_LIT(KMouse, "mouse\n");
			logger->log(KMouse);
		}
		else if (XY_input_type == EXYInputDeltaMouse)
		{
			_LIT(KDeltaMouse, "delta mouse\n");
			logger->log(KDeltaMouse);
		}

		_LIT(KSize, "Size:\n");

		TSize XY_input_size = machine_info.iXYInputSizeInPixels;
		logger->log(KSize);
		logger->log(KPixelWidth, XY_input_size.iWidth);
		logger->log(KPixelHeight, XY_input_size.iHeight);
	}
	else
	{
		_LIT(KNotAvailable, "Not available.\n\n");
		logger->log(KNotAvailable);
	}

	TMachineStartupType startup_type;
	UserHal::StartupReason(startup_type);

	_LIT(KMachineInfo, "Machine info:\n");
	_LIT(KStartupType, "Startup type:");

	logger->log(KMachineInfo);
	logger->log(KStartupType);

	if (startup_type == EStartupCold)
	{
		_LIT(KCold, "cold\n");
		logger->log(KCold);
	}
	else if (startup_type == EStartupColdReset)
	{
		_LIT(KColdReset, "cold reset\n");
		logger->log(KColdReset);
	}
	else if (startup_type == EStartupNewOs)
	{
		_LIT(KNewOS, "new OS\n");
		logger->log(KNewOS);
	}
	else if (startup_type == EStartupPowerFail)
	{
		_LIT(KPowerFailure, "power failure\n");
		logger->log(KPowerFailure);
	}
	else if (startup_type == EStartupWarmReset)
	{
		_LIT(KWarmReset, "warm reset\n");
		logger->log(KWarmReset);
	}
	else if (startup_type == EStartupKernelFault)
	{
		_LIT(KKernelFault, "kernel fault\n");
		logger->log(KKernelFault);
	}
	else if (startup_type == EStartupSafeReset)
	{
		_LIT(KSafeReset, "safe reset\n");
		logger->log(KSafeReset);
	}

	_LIT(KKeyboard, "Keyboard:%s\n");
	_LIT(KBacklight, "Backlight:%s\n");
	_LIT(KKeyboardID, "Keyboard ID:%d\n");
	_LIT(KDisplayID, "Display ID:%d\n");
	_LIT(KMachineUID, "Machine UID:0x%X\n");
	_LIT(KLEDCapabilities, "LED capabilities:%d\n");
	_LIT(KProcessorClock, "Processor clock:%dkHz\n");
	_LIT(KClockFactor, "Clock factor:%d\n");
	_LIT(KMaxColours, "Max colours:%d\n\n");

	logger->log(KKeyboard, machine_info.iKeyboardPresent ? "yes" : "no");
	logger->log(KBacklight, machine_info.iBacklightPresent ? "yes" : "no");
	logger->log(KKeyboardID, machine_info.iKeyboardId);
	logger->log(KDisplayID, machine_info.iDisplayId);
	logger->log(KMachineUID, machine_info.iMachineUniqueId); // This is actually not unique
	logger->log(KLEDCapabilities, machine_info.iLedCapabilities);
	logger->log(KProcessorClock, machine_info.iProcessorClockInKHz);
	logger->log(KClockFactor, machine_info.iSpeedFactor);
	logger->log(KMaxColours, machine_info.iMaximumDisplayColors);
}

void print_memory_info()
{
	TPckgBuf<TMemoryInfoV1> memory_info_buffer;
	UserHal::MemoryInfo(memory_info_buffer);
	TMemoryInfoV1& memory_info = memory_info_buffer();

	TInt page_size;
	UserHal::PageSizeInBytes(page_size);

	_LIT(KMemoryInfo, "Memory info:\n");
	_LIT(KTotalRAM, "Total RAM:0x%X\n");
	_LIT(KTotalROM, "Total ROM:0x%X\n");
	_LIT(KPageSize, "Page size:%d\n");
	_LIT(KMaximumFreeRAM, "Max free RAM:0x%X\n");
	_LIT(KFreeRAM, "Free RAM:0x%X\n");
	_LIT(KInternalRAM, "Internal RAM:0x%X\n");
	_LIT(KROMReprogammable, "ROM reprogrammable:%s\n\n");

	logger->log(KMemoryInfo);
	logger->log(KTotalRAM, memory_info.iTotalRamInBytes);
	logger->log(KTotalROM, memory_info.iTotalRomInBytes);
	logger->log(KPageSize, page_size);
	logger->log(KMaximumFreeRAM, memory_info.iMaxFreeRamInBytes);
	logger->log(KFreeRAM, memory_info.iFreeRamInBytes);
	logger->log(KInternalRAM, memory_info.iInternalDiskRamInBytes);
	logger->log(KROMReprogammable, memory_info.iRomIsReprogrammable ? "yes" : "no");
}

void print_device_info()
{
	TInt manufacturer, hardware_rev, software_rev, software_build, family_revision, model, machine_uid;
	HAL::Get(HALData::EManufacturer, manufacturer);
	HAL::Get(HALData::EManufacturerHardwareRev, hardware_rev);
	HAL::Get(HALData::EManufacturerSoftwareRev, software_rev);
	HAL::Get(HALData::EManufacturerSoftwareBuild, software_build);
	HAL::Get(HALData::EDeviceFamilyRev, family_revision);
	HAL::Get(HALData::EModel, model);
	HAL::Get(HALData::EMachineUid, machine_uid);

	TVersion fw_version = User::Version();

	_LIT(KDeviceInfo, "Device info:\n");
	_LIT(KFirmware, "Firmware:v%d.%d.%d\n");
	_LIT(KManufacturer, "Manufacturer:0x%X\n");
	_LIT(KHardwareRevision, "Hardware revision:0x%X\n");
	_LIT(KSoftwareRevision, "Software revision:0x%X\n");
	_LIT(KSoftwareBuild, "Software build:0x%X\n");
	_LIT(KFamilyRevision, "Family revision:0x%X\n");
	_LIT(KModel, "Model:0x%X\n");
	_LIT(KMachineUID, "Machine UID:0x%X\n\n");

	logger->log(KDeviceInfo);
	logger->log(KFirmware, fw_version.iMajor, fw_version.iMinor, fw_version.iBuild);
	logger->log(KManufacturer, manufacturer);
	logger->log(KHardwareRevision, hardware_rev);
	logger->log(KSoftwareRevision, software_rev);
	logger->log(KSoftwareBuild, software_build);
	logger->log(KFamilyRevision, family_revision);
	logger->log(KModel, model);
	logger->log(KMachineUID, machine_uid);
}

void print_drive_info()
{
	TPckgBuf<TDriveInfoV1> drive_info_buffer;
	UserHal::DriveInfo(drive_info_buffer);
	TDriveInfoV1& drive_info = drive_info_buffer();

	_LIT(KDriveInfo, "Drive info:\n");
	_LIT(KTotalDrives, "Total drives:%d\n");
	_LIT(KTotalSockets, "Total sockets:%d\n");
	_LIT(KRugged, "Rugged:%d\n\n");
	_LIT(KDriveNames, "Drive names:\n");
	_LIT(KSocketNames, "\nSocket names:\n");
	_LIT(KNameFormat, "%d:%s\n");
	_LIT(KNewline, "\n");

	logger->log(KDriveInfo);
	logger->log(KTotalDrives, drive_info.iTotalSupportedDrives);
	logger->log(KTotalSockets, drive_info.iTotalSockets);
	logger->log(KRugged, drive_info.iRuggedFileSystem);

	logger->log(KDriveNames);
	for (TInt i = 0; i < KMaxLocalDrives; ++i)
	{
		logger->log(KNameFormat, i, drive_info.iDriveName[i].PtrZ());
	}

	logger->log(KSocketNames);
	for (TInt i = 0; i < KMaxPBusSockets; ++i)
	{
		logger->log(KNameFormat, i, drive_info.iSocketName[i].PtrZ());
	}
	logger->log(KNewline);
}

void print_rom_info()
{
	TPckgBuf<TRomInfoV1> ROM_info;
	UserHal::RomInfo(ROM_info);

	for (TInt i = 0; i < KMaxRomDevices; ++i)
	{
		_LIT(KROMFormat, "ROM %d:\n");
		_LIT(KType, "Type:%d\n");
		_LIT(KSize, "Size:0x%X\n");
		_LIT(KBusWidth, "Bus width:%d\n");
		_LIT(KSpeed, "Speed:0x%X\n\n");

		TRomInfoEntryV1& current_entry = ROM_info().iEntry[i];
		logger->log(KROMFormat, i + 1);
		logger->log(KType, current_entry.iType);
		logger->log(KSize, current_entry.iSize);
		logger->log(KBusWidth, current_entry.iWidth);
		logger->log(KSpeed, current_entry.iSpeed);
	}
}

void print_register_info()
{
	SRegisterInfo register_info;
	RDebug::RegisterInfo(register_info);

	_LIT(KRegisterInfo, "Register info:\n");
	_LIT(KRegisterCount, "Register count:%d\n");
	_LIT(KPCRegister, "PC register:%d\n");
	_LIT(KStatusRegister, "Status register:%d\n");
	_LIT(KThreadStartRegister, "Thread start reg:%d\n\n");

	logger->log(KRegisterInfo);
	logger->log(KRegisterCount, register_info.iNumberOfRegisters);
	logger->log(KPCRegister, register_info.iNumberOfPcRegister);
	logger->log(KStatusRegister, register_info.iNumberOfStatusRegister);
	logger->log(KThreadStartRegister, register_info.iThreadStartRegister);
}

TInt Main()
{
	_LIT(KLogPath, "E:\\SYMBIAN_DUMPER.log");
	_LIT(KConsoleName, "Symbian dumper");
	_LIT(KDumperInfo, "Symbian dumper v3.1\nby tambre\n\n");
	_LIT(KDone, "Done!");

	// Create the logger
	logger = Logger::NewL(KLogPath, KConsoleName);
	logger->log(KDumperInfo);

	// Print various information about the device
	print_machine_info();
	print_memory_info();
	print_device_info();
	print_drive_info();
	print_rom_info();
	print_register_info();

	// Dump known memory areas
	dump_ROM();
	dump_BOOT();
	dump_ROOT();

	// The first one results in a page fault. The second one results in an access violation.
	// Conclusion: There is some virtual memory protection for certain regions of memory.
	// TODO: Find a way to access those regions.
	//logger->log(_L("0x3FFFFFFC:0x%X\n\n"), *reinterpret_cast<TUint32*>(0x3FFFFFFC)); // Results in a page fault
	//logger->log(_L("0x40000000:0x%X\n\n"), *reinterpret_cast<TUint32*>(0x40000000)); // Results in an access violation

	logger->log(KDone);
	logger->get_char();

	return 0;
}